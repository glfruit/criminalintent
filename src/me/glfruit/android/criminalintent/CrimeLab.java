package me.glfruit.android.criminalintent;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.content.Context;
import android.util.Log;

public class CrimeLab {

	private static final String TAG = "CrimeLab";

	private static final String FILENAME = "crimes.json";

	private static CrimeLab crimeLab;

	private CriminalIntentJSONSerializer mSerializer;

	private Context mAppContext;

	private List<Crime> crimes;

	private CrimeLab(Context appContext) {
		mAppContext = appContext;
		initCrimes();
	}

	public boolean saveCrimes() {
		try {
			mSerializer.saveCrimes(crimes);
			Log.d(TAG, "crimes saved to file");
			return true;
		} catch (Exception e) {
			Log.e(TAG, "Error saving crimes:", e);
			return false;
		}
	}

	public void deleteCrime(Crime crime) {
		crimes.remove(crime);
	}

	private void initCrimes() {
		mSerializer = new CriminalIntentJSONSerializer(mAppContext, FILENAME);
		try {
			crimes = mSerializer.loadCrimes();
		} catch (Exception e) {
			crimes = new ArrayList<Crime>();
			Log.e(TAG, "Error loading crimes: ", e);
		}
	}

	public static CrimeLab get(Context c) {
		if (crimeLab == null) {
			crimeLab = new CrimeLab(c.getApplicationContext());
		}
		return crimeLab;
	}

	public List<Crime> getCrimes() {
		return crimes;
	}

	public Crime getCrime(UUID id) {
		for (Crime crime : crimes) {
			if (crime.getId().equals(id)) {
				return crime;
			}
		}
		return null;
	}

	public void addCrime(Crime c) {
		crimes.add(c);
	}

}
